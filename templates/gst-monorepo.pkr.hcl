packer {
  required_plugins {
    tart = {
      version = ">= 1.2.0"
      source  = "github.com/cirruslabs/tart"
    }
  }
}

variable "macos_version" {
  type =  string
  default = "ventura"
}

variable "xcode_version" {
  type =  string
  default = "15"
}

variable "gstreamer_version" {
  type = string
  default = "test"
}

source "tart-cli" "tart" {
  vm_base_name = "${var.macos_version}-xcode:${var.xcode_version}"
  vm_name      = "${var.macos_version}-gstreamer:${var.gstreamer_version}"
  cpu_count    = 4
  memory_gb    = 8
  disk_size_gb = 70
  headless     = true
  ssh_password = "gstreamer"
  ssh_username = "gst-ci"
  ssh_timeout  = "120s"
}

build {
  sources = ["source.tart-cli.tart"]

  provisioner "shell" {
    inline = [
      "export PATH=$PATH:/Users/gst-ci/Library/Python/3.9/bin",
      "echo 'export PATH=$PATH:/Users/gst-ci/Library/Python/3.9/bin' >> ~/.zprofile",
      "pip3 install --upgrade pip",
      "pip3 install -U meson ninja",
      "git clone https://gitlab.freedesktop.org/gstreamer/gstreamer.git",
      "cd gstreamer",
      "git submodule update --init --depth=1",
      "meson subprojects download",
      "./ci/scripts/handle-subprojects-cache.py --cache-dir /Users/gst-ci/subprojects --build subprojects/",
      "cd ..",
      "rm -rf gstreamer",
    ]
  }
}
