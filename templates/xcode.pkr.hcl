packer {
  required_plugins {
    tart = {
      version = ">= 1.12.0"
      source  = "github.com/cirruslabs/tart"
    }
  }
}

variable "macos_version" {
  type = string
}

variable "xcode_version" {
  type = string
}

variable "disk_free_mb" {
  type = number
  default = 15000
}

source "tart-cli" "tart" {
  vm_base_name = "${var.macos_version}-base"
  // use tag or the last element of the xcode_version list
  vm_name      = "${var.macos_version}-xcode:${var.xcode_version}"
  cpu_count    = 4
  memory_gb    = 8
  disk_size_gb = 70
  headless     = true
  ssh_password = "gstreamer"
  ssh_username = "gst-ci"
  ssh_timeout  = "120s"
}

build {
  sources = ["source.tart-cli.tart"]

  provisioner "file" {
    source      = "Xcode_${var.xcode_version}.xip"
    destination = "~/Xcode_${var.xcode_version}.xip"
  }

  provisioner "shell" {
    inline = [
      "sudo echo installing XCode ${var.xcode_version}",
      "xip -x Xcode_${var.xcode_version}.xip",
      "ls",
      "rm Xcode_${var.xcode_version}.xip",
      "sudo mv Xcode.app /Applications/Xcode.app",
      "xcode-select -p",
      "sudo xcodebuild -license accept",
      "sudo xcodebuild -runFirstLaunch",
      "sudo xcodebuild -downloadPlatform iOS",
      "xcodebuild -version",
    ]
  }

  // check there is at least 15GB of free space and fail if not
  provisioner "shell" {
    inline = [
      "source ~/.zprofile",
      "df -h",
      "export FREE_MB=$(df -m | awk '{print $4}' | head -n 2 | tail -n 1)",
      "[[ $FREE_MB -gt ${var.disk_free_mb} ]] && echo OK || exit 1"
    ]
  }

  // some other health checks
  provisioner "shell" {
    inline = [
      "source ~/.zprofile",
    ]
  }
}
