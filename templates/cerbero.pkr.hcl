packer {
  required_plugins {
    tart = {
      version = ">= 1.2.0"
      source  = "github.com/cirruslabs/tart"
    }
  }
}

variable "macos_version" {
  type =  string
  default = "ventura"
}

variable "xcode_version" {
  type =  string
  default = "15"
}

variable "cerbero_version" {
  type = string
  default = "test"
}

source "tart-cli" "tart" {
  vm_base_name = "${var.macos_version}-xcode:${var.xcode_version}"
  vm_name      = "${var.macos_version}-cerbero:${var.cerbero_version}"
  cpu_count    = 4
  memory_gb    = 8
  disk_size_gb = 120
  headless     = true
  ssh_password = "gstreamer"
  ssh_username = "gst-ci"
  ssh_timeout  = "120s"
}

build {
  sources = ["source.tart-cli.tart"]

  provisioner "shell" {
    inline = [
      "/usr/sbin/softwareupdate --install-rosetta --agree-to-license",
    ]
  }

  provisioner "shell" {
    inline = [
      "curl -O -L https://tukaani.org/xz/xz-5.4.3.tar.gz",
      "tar xvf xz-5.4.3.tar.gz",
      "rm xz-5.4.3.tar.gz",
      "cd xz-5.4.3",
      "./configure --prefix /usr/local",
      "make",
      "sudo make install",
      # move liblzma.5.dylib out of the default search path
      "sudo install_name_tool -id '@rpath/liblzma.5.dylib' /usr/local/lib/liblzma.5.dylib",
      "sudo mkdir -p /usr/local/bin/lib",
      "sudo mv /usr/local/lib/liblzma.5.dylib /usr/local/bin/lib/liblzma.5.dylib",
      "sudo rm -rf /usr/local/lib/liblzma*",
      "sudo rm -rf /usr/local/include/lzma*",
      "for bin in xz lzmadec lzmainfo xzdec xzdec; do sudo install_name_tool -change /usr/local/lib/liblzma.5.dylib '@rpath/liblzma.5.dylib' /usr/local/bin/$bin ; sudo install_name_tool -add_rpath '@loader_path/lib' /usr/local/bin/$bin || true ; done",
      "cd ..",
      "rm -rf xz-5*",
      "cd $HOME",
      "git clone https://gitlab.freedesktop.org/gstreamer/cerbero.git",
      "cd cerbero",
      "echo 'local_sources=\"/Users/gst-ci/cerbero/cerbero-sources\"' > localconf.cbc",
      "echo 'home_dir=\"/Users/gst-ci/cerbero/cerbero-build\"' >> localconf.cbc",
      "cat localconf.cbc",
      "./cerbero-uninstalled -t -c localconf.cbc -c config/cross-macos-universal.cbc fetch-bootstrap",
      "./cerbero-uninstalled -t -c localconf.cbc -c config/cross-macos-universal.cbc fetch-package gstreamer-1.0",
      "./cerbero-uninstalled -t -c localconf.cbc -c config/cross-ios-universal.cbc fetch-bootstrap",
      "./cerbero-uninstalled -t -c localconf.cbc -c config/cross-ios-universal.cbc fetch-package gstreamer-1.0",
      "rm -rf /Users/gst-ci/cerbero/cerbero-build/{dist,logs,sources}",
      "rm -f /Users/gst-ci/cerbero/cerbero-build/{darwin,ios}*.cache",
    ]
  }
}
